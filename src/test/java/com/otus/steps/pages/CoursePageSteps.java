package com.otus.steps.pages;

import com.google.inject.Inject;
import com.otus.pages.CoursePage;
import io.cucumber.java.en.Then;

public class CoursePageSteps {
  @Inject
  public CoursePage coursePage;

  @Then("^The (.*) course page should be opened")
  public void pageShouldBeTheSameAs(String course){coursePage.headerShouldBeTheSameAs(course);}

  @Then("^The (.*) course should be opened")
  public void courseShouldBeOpened(String title){
    coursePage.titleShouldBeTheSameAs(title);
  }

}
