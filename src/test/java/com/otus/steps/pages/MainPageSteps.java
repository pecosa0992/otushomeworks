package com.otus.steps.pages;
import com.google.inject.Inject;
import com.otus.pages.MainPage;
import io.cucumber.java.en.And;
import io.cucumber.java.en.When;


public class MainPageSteps {
  @Inject
  public MainPage mainPage;

  @And("Open the main page")
  public void openMainPage() {
    mainPage.open();
  }


}
