package com.otus.steps.pages;

import com.google.inject.Inject;
import com.otus.componenet.CoursesComponent;
import io.cucumber.java.en.When;

public class CoursesComponentSteps {
  @Inject
  public CoursesComponent coursesComponent;

  @When("^Click on (.*) course page")
  public void clickOnEarlierCourse(String time){
    coursesComponent.clickEarliestLatestCourse(time);}

  @When("Click on {string} course")
  public void clickOnCourse(String course){
    coursesComponent.filterCoursesByName(course);
  }
}
