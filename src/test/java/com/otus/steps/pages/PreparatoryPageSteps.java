package com.otus.steps.pages;

import com.google.inject.Inject;
import com.otus.pages.PreparatoryCoursesPage;
import io.cucumber.java.en.And;

public class PreparatoryPageSteps {
  @Inject
  PreparatoryCoursesPage preparatoryCoursesPage;

  @And("Open the  Preparatory courses page")
  public void openPreparatoryCoursesPage() {
    preparatoryCoursesPage.open();
  }
}
