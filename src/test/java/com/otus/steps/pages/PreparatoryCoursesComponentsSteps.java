package com.otus.steps.pages;

import com.google.inject.Inject;
import com.otus.componenet.PreparatoryCoursesComponents;
import io.cucumber.java.en.When;

public class PreparatoryCoursesComponentsSteps {
  @Inject
  public PreparatoryCoursesComponents preparatoryCoursesComponents;


  @When("^Click on (.*) course page in Preparatory courses page")
  public void clickOnCheapExpensiveCourse(String course) {
    preparatoryCoursesComponents.chooseCheapExpensiveCourse(course);
  }
}