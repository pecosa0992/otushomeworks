package com.otus.steps.common;

import com.google.inject.Inject;
import com.otus.di.GuiceScooped;
import com.otus.driver.factory.DriverFactory;
import io.cucumber.java.en.Given;


public class CommonSteps {
  @Inject
  public GuiceScooped guiceScooped;

  @Given("Open the browser {string}")
  public void open_browser(String browser) {
    guiceScooped.driver=new DriverFactory().getDriver(browser);
  }

}