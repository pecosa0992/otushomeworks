package com.otus;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(plugin = {"pretty"},tags = "",features = "../otus_homeworks/src/test/java/resources")
public class Runner_Test {
}
