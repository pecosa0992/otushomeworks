Feature: Click on the course component
  Scenario Outline: Click the earlier/latest course on the main page
    Given Open the browser "chrome"
    And   Open the  Preparatory courses page
    When  Click on <price> course page in Preparatory courses page
    Then  The <courses> course should be opened

    Examples:
      | price     | courses                                            |
      | cheapest  | Выбор профессии в IT                               |
      | expensive | C# для начинающих программистов. Академия искусств |