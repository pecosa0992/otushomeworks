Feature: Click on the course component
 Scenario Outline: Click the earlier/latest course on the main page
    Given Open the browser "chrome"
    And   Open the main page
    When  Click on <start_date> course page
    Then  The <courses> course page should be opened


    Examples:
      | start_date | courses                   |
      | earliest   | Системный аналитик. Basic |
      | latest     | C# Developer              |


  Scenario: Click the course on the main page
    Given Open the browser "chrome"
    And   Open the main page
    When  Click on "Специализация С++" course
    Then  The C++ DEVELOPER course page should be opened

