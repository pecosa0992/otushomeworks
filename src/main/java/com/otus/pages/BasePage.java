package com.otus.pages;

import com.google.inject.Inject;
import com.otus.annotations.Path;
import com.otus.di.GuiceScooped;
import com.otus.pageobject.AbsPageObject;

public class BasePage<T> extends AbsPageObject<T> {

  private final String baseUrl = System.getProperty("webdriver.base.url", "https://otus.ru");


  @Inject
  public BasePage(GuiceScooped guiceScooped) {
    super(guiceScooped);
  }



  private String getPath() {
    Path path = getClass().getAnnotation(Path.class);
    if (path != null) {
      return path.value();
    }
    return "";
  }


  public T open() {
    guiceScooped.driver.get(baseUrl + getPath());
    return (T) this;
  }
}
