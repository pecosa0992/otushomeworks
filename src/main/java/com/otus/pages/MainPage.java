package com.otus.pages;
import com.google.inject.Inject;
import com.otus.annotations.Path;
import com.otus.di.GuiceScooped;
import org.openqa.selenium.By;

@Path("/")
public class MainPage extends BasePage<MainPage> {

  @Inject
  public MainPage(GuiceScooped guiceScooped) {
    super(guiceScooped);
  }

  private final String lessonsLocator = "//div[contains(text(),'%s')]/ancestor::a";

  public MainPage moveToTheCourse(String courseName) {
    String selector = String.format(lessonsLocator, courseName);
    this.actions.moveToElement($(By.xpath(selector))).click().build().perform();
    return this;
  }


}
