package com.otus.data;

public enum MainPageLessonsData {
  ApacheKafkaLesson("Apache Kafka"),
  SystemAnalyst("Системный аналитик. Advanced"),
  JavaSpecialization("Специализация Java-разработчик"),
  AndroidSpecialization("Специализация Android"),
  QAAutomationSpecialization("Специализация QA Automation Engineer"),
  FullstackDeveloperSpecialization("Специализация Fullstack Developer"),
  CPlusSpecialization("Специализация С++"),
  CSharpSpecialization("Специализация С#"),
  AdministratorLinuxSpecialization("Специализация Administrator Linux"),
  SystemAnalystSpecialization("Специализация Системный аналитик"),
  PythonSpecialization("Специализация Python"),
  MachineLearningSpecialization("Специализация Machine Learning"),
  IOSSpecialization("Специализация iOS Developer"),
  PHPSpecialization("Специализация PHP Developer"),
  NetworkEngineerSpecialization("Специализация сетевой инженер"),
  IT_Recruiter("IT-Recruiter"),
  AndroidDeveloperBasic("Android Developer. Basic"),
  AndroidSpecializationDeveloper("Специализация Android-разработчик");


  private final String name;

  MainPageLessonsData(String name) {
    this.name = name;
  }

  public String getName() {
    return name;
  }

}
