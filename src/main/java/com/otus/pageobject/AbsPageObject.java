package com.otus.pageobject;

import com.google.inject.Inject;
import com.otus.data.HeadersOfThePagesData;
import com.otus.di.GuiceScooped;
import org.junit.jupiter.api.Assertions;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public abstract class AbsPageObject<T> {
  protected WebDriver driver;
  protected Actions actions;
  protected JavascriptExecutor js;

  @Inject
  public GuiceScooped guiceScooped;

  @Inject
  public AbsPageObject(GuiceScooped guiceScooped) {
    this.guiceScooped = guiceScooped;
    this.driver= guiceScooped.driver;
    this.actions = new Actions(this.guiceScooped.driver);
    this.js = (JavascriptExecutor)this.guiceScooped.driver;
    PageFactory.initElements(this.guiceScooped.driver, this);
  }

  @FindBy(tagName = "h1")
  private WebElement header;

  public T headerShouldBeTheSameAs(String header) {
    Assertions.assertEquals(header, this.header.getText());
    return (T) this;
  }

  @FindBy(tagName = "h3")
  private WebElement header2;

  public T titleShouldBeTheSameAs(String header) {
    Assertions.assertEquals(header, this.header2.getText());
    return (T) this;
  }


  public WebElement $(By locator) {
    return driver.findElement(locator);
  }
}

