package com.otus.driver.factory;

import com.otus.driver.impl.ChromeWebDriver;
import com.otus.driver.impl.FirefoxWebDriver;
import com.otus.driver.impl.OperaWebDriver;
import com.otus.exceptions.DriverTypeNotSupported;
import org.openqa.selenium.support.events.EventFiringWebDriver;

import java.util.Locale;

public class DriverFactory implements IDriverFactory {

  @Override
  public EventFiringWebDriver getDriver(String browserName) {
    switch (browserName) {
      case "chrome": {
        return new EventFiringWebDriver(new ChromeWebDriver().newDriver());
      }
      case "firefox": {
        return new EventFiringWebDriver(new FirefoxWebDriver().newDriver());
      }
      case "opera": {
        return new EventFiringWebDriver(new OperaWebDriver().newDriver());
      }
      default:
        try {
          throw new DriverTypeNotSupported(browserName);
        } catch (DriverTypeNotSupported ex) {
          ex.printStackTrace();
          return null;
        }
    }
  }
}
