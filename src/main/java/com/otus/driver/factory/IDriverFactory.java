package com.otus.driver.factory;

import com.otus.exceptions.DriverTypeNotSupported;
import org.openqa.selenium.support.events.EventFiringWebDriver;

public interface IDriverFactory {
  EventFiringWebDriver getDriver(String browserName) throws DriverTypeNotSupported;
}
