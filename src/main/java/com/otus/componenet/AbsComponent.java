package com.otus.componenet;

import com.google.inject.Inject;
import com.otus.di.GuiceScooped;
import org.junit.jupiter.api.Assertions;
import org.openqa.selenium.WebDriver;
import com.otus.pageobject.AbsPageObject;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class AbsComponent<T> extends AbsPageObject<T> {
  @Inject
  public AbsComponent(GuiceScooped guiceScooped) {
    super(guiceScooped);
  }


}
