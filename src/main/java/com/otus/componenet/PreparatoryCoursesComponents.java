package com.otus.componenet;

import com.google.inject.Inject;
import com.otus.di.GuiceScooped;
import com.otus.pages.BasePage;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.AbstractMap;
import java.util.List;
import java.util.Optional;
import java.util.function.BinaryOperator;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PreparatoryCoursesComponents extends BasePage<PreparatoryCoursesComponents> {
  @Inject
  public PreparatoryCoursesComponents(GuiceScooped guiceScooped) {
    super(guiceScooped);
  }

  @FindBy(css = ".lessons__new-item-price")
  private List<WebElement> lessonsItemPrice;

  public PreparatoryCoursesComponents getExpensiveCheapCourseItem(BinaryOperator<AbstractMap.SimpleEntry<Integer, WebElement>> compareFunction) {
    Optional<WebElement> course = lessonsItemPrice.stream().map(element -> {
      String str = element.getText();
      Pattern pattern = Pattern.compile("\\d+");
      Matcher matcher = pattern.matcher(str);
      int day2 = 0;
      if (matcher.find()) {
        String day = matcher.group();
        day2 = Integer.parseInt(day);
      }
      return new AbstractMap.SimpleEntry<>(day2, element);
    }).reduce(compareFunction)
        .map(AbstractMap.SimpleEntry::getValue);
    course.ifPresent(WebElement::click);
    return this;
  }

  public static AbstractMap.SimpleEntry<Integer, WebElement> findCheapCourse(AbstractMap.SimpleEntry<Integer, WebElement> entry1, AbstractMap.SimpleEntry<Integer, WebElement> entry2) {
    if (entry1.getKey() < entry2.getKey()) {
      return entry1;
    } else {
      return entry2;
    }
  }

  public static AbstractMap.SimpleEntry<Integer, WebElement> findExpensiveCourse(AbstractMap.SimpleEntry<Integer, WebElement> entry1, AbstractMap.SimpleEntry<Integer, WebElement> entry2) {
    if (entry1.getKey() > entry2.getKey()) {
      return entry1;
    } else {
      return entry2;
    }
  }

  public void chooseCheapExpensiveCourse(String time) {
    if (time.equals("cheapest")) {
      getExpensiveCheapCourseItem(PreparatoryCoursesComponents::findCheapCourse);
    } else if (time.equals("expensive")) {
      getExpensiveCheapCourseItem(PreparatoryCoursesComponents::findExpensiveCourse);
    } else {
      System.out.println("Wrong price was entered");
    }
  }

}
